#include "mapper2d_params.h"

namespace srrg_mapper2d {

  Mapper2DParams::Mapper2DParams(Mapper2D* mapper_){
    _mapper = mapper_;
      
    //Default values
    _closures_inlier_threshold = 3.0;
    _closures_window = 10;
    _closures_min_inliers = 6;
    _lcaligner_inliers_distance = 0.05;
    _lcaligner_min_inliers_ratio = 0.5;
    _lcaligner_min_num_correspondences = 50;
    _num_matching_beams = 0.0;
    _matching_fov = 0.0;

    _verbose = false;
    _log_times = false;
    _use_merger = false;
    _vertex_translation_threshold = 0.5;
    _vertex_rotation_threshold = M_PI_4;
      
  }
    
  void Mapper2DParams::applyParams(){
    if (!_mapper)
      return;
      
    _mapper->graphMap()->setLoopClosureInlierThreshold(_closures_inlier_threshold);
    _mapper->graphMap()->setLoopClosureWindow(_closures_window);
    _mapper->graphMap()->setLoopClosureMinInliers(_closures_min_inliers);

    _mapper->graphMap()->setInliersDistance(_lcaligner_inliers_distance);
    _mapper->graphMap()->setMinInliersRatio(_lcaligner_min_inliers_ratio);
    _mapper->graphMap()->setMinNumCorrespondences(_lcaligner_min_num_correspondences);
  
    _mapper->graphMap()->setNumRanges(_num_matching_beams);
    _mapper->graphMap()->setFov(_matching_fov);

    _mapper->setVerbose(_verbose);
    _mapper->setLogTimes(_log_times);
    _mapper->setUseMerger(_use_merger);
    _mapper->setVertexTranslationThreshold(_vertex_translation_threshold);
    _mapper->setVertexRotationThreshold(_vertex_rotation_threshold);
    
  }


}
